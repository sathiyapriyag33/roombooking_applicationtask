import React from 'react';
import Rooms from './Rooms';
import Adults from './Adults';
import Children from './Children';
import './booking.css';

class Booking extends React.Component {
  constructor(props) {
    super(props);
    this.counterElement1 = React.createRef();
    this.counterElement2 = React.createRef();
    this.counterElement3 = React.createRef();
  }


  render() {
    return (
      <div class="room-book">
          <h3> ROOM BOOKING </h3>

          <p class="room-head"> <i class="fa fa-users"> </i> Choose number of
            <strong> people </strong> </p>
         
            <div class="room-category">

          <div class="room-format"> 
          <i class="fa fa-bed"></i> ROOMS 
          <Rooms  ref={this.counterElement1}  />
          </div>
          <div class="room-format"> 
          <i class="fa fa-user"></i> ADULTS 
          <Adults  ref={this.counterElement2}/>
          </div>
          <div class="room-made"> 
          <i class="fa fa-child"></i> CHILDREN 
          <Children  ref={this.counterElement3}/>
          </div>

          </div>

      </div>
    );
  }
}

export default Booking;