import React from 'react';


class Adults extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          adults: 1,
          show: true
        };
      }


      handleDecrease = () => {
        console.log("hello decrease")
        if(this.state.adults < 1){
            this.setState({
                adults:0
            });
          }else {
        this.setState(prevState => ({  adults: Math.max(prevState.adults - 1, 1) }));
          }
      }

    handleIncrease = () => {
        console.log("hello increase")
        if(this.state.adults < 4) {
          this.setState({
          adults: this.state.adults + 1, 
           message: null
          });
        } else {
          this.setState({
            message: null
          });
    }
    }

    render() {
        return (
            
            <div>
                <span class="fa fa-minus-circle" onClick={this.handleDecrease}></span>
                { this.state.show ? <span class="room-count">{ this.state.adults }</span> : '' }
                <span class="fa fa-plus-circle" onClick={this.handleIncrease}></span>
            </div>
        )
    }
}

export default Adults;