import React from 'react';


class Children extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          children: 1,
          show: true
        };
      }


      handleDecrease = () => {
        console.log("hello decrease")
        if(this.state.children < 1){
            this.setState({
                children:0
            });
          }else {
        this.setState(prevState => ({  children: Math.max(prevState.children - 1, 1) }));
          }
      }

    handleIncrease = () => {
        console.log("hello increase")
        if(this.state.children < 4) {
          this.setState({
          children: this.state.children + 1,
           message: null
          });
        } else {
          this.setState({
            message: null
          });
    }
    }

    render() {
        return (
            
            <div>
                <span class="fa fa-minus-circle" onClick={this.handleDecrease}></span>
                { this.state.show ? <span class="room-count">{ this.state.children }</span> : '' }
                <span class="fa fa-plus-circle" onClick={this.handleIncrease}></span>
            </div>
        )
    }
}

export default Children;