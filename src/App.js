import React, {Component} from 'react';
import Booking from './Booking';
import './booking.css';
  
class App extends React.Component{
  render() {
    return(
      <div className="App">
        <Booking />

      </div>
    );
  }
}
  
export default App;

