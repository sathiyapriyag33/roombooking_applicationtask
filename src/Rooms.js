import React from 'react';

class Rooms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          rooms: 1,
          show: true
        };
      }


    handleDecrease = () => {
        console.log("hello decrease")
        if(this.state.rooms < 1){
            this.setState({
              rooms:0
            });
          }else {
        this.setState(prevState => ({  rooms: Math.max(prevState.rooms - 1, 1) }));
          }
      }

    handleIncrease = () => {
        console.log("hello increase")
        if(this.state.rooms < 5) {
          this.setState({
           rooms: this.state.rooms + 1,
           message: null
          });
        } else {
          this.setState({
            message: null
          });
    }
    }

    render() {
        return (
            
            <div>
                <span class="fa fa-minus-circle" onClick={this.handleDecrease}></span>
                { this.state.show ? <span class="room-count">{ this.state.rooms }</span> : '' }
                <span class="fa fa-plus-circle" onClick={this.handleIncrease}></span>
            </div>
        )
    }
}

export default Rooms;
